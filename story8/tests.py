from django.test import TestCase, LiveServerTestCase
from django.test.client import Client
from django.urls import resolve
from .views import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import time


# Create your tests here.
class UnitTest(TestCase):
    def test_index_page_url_exists(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_index_template_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
    
    def test_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.driver = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest,self).setUp()

    def tearDown(self):
        self.driver.quit()
        super(FunctionalTest,self).tearDown()
    
    def test_accordion(self):
        self.driver.get(self.live_server_url)
        response_page = self.driver.page_source

        self.assertIn("Get to know me?", response_page)
        self.assertIn("My activity?", response_page)
        self.assertIn("My experience and achievement?", response_page)
        self.assertIn("My skills?", response_page)
    
    def test_accordion_content(self):
        self.driver.get(self.live_server_url)
        response_content = self.driver.page_source

        accordion_bar = self.driver.find_element_by_class_name('col-sm-10')
        self.assertFalse(accordion_bar.get_attribute('aria-expanded'))

        accordion_bar.click()
        time.sleep(3)
        self.assertTrue(accordion_bar.get_attribute('aria-expanded'))
    
    def test_up_down_arrows(self) :
        self.driver.get(self.live_server_url)
        response_page = self.driver.page_source

        up = self.driver.find_element_by_id('oneUp')
        down = self.driver.find_element_by_id('oneDown')

        know = response_page.find('Get to know me?')
        activity = response_page.find('My activity?')
        self.assertTrue(know < activity)

        time.sleep(2)
        down.click()
        response_page = self.driver.page_source

        time.sleep(2)
        getToKnowMe = response_page.find('Get to know me?')
        activity = response_page.find('My activity?')
        self.assertTrue(getToKnowMe > activity)

        time.sleep(2)
        up.click()

        time.sleep(2)
        response_page = self.driver.page_source
        getToKnowMe = response_page.find('Get to know me?')
        activity = response_page.find('My activity?')
        self.assertTrue(getToKnowMe < activity)
    
    def test_change_theme(self) :
        self.driver.get(self.live_server_url)
        response_content = self.driver.page_source

        background = self.driver.find_element_by_tag_name("body").value_of_css_property("background-color")
        accordion = self.driver.find_element_by_class_name("accordion").value_of_css_property("background-color")

        self.assertEqual("rgba(244, 218, 218, 1)", background)
        self.assertEqual("rgba(144, 48, 61, 1)", accordion)

        self.driver.find_element_by_class_name("nightbutton").click()

        background = self.driver.find_element_by_tag_name("body").value_of_css_property("background-color")
        accordion = self.driver.find_element_by_class_name("accordion").value_of_css_property("background-color")

        self.assertEqual("rgba(178, 235, 242, 1)", background)
        self.assertEqual("rgba(140, 52, 65, 1)", accordion)








